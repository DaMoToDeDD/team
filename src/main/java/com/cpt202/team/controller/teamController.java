package com.cpt202.team.controller;

import com.cpt202.team.models.Team;

//import java.lang.reflect.Array;
//import com.cpt202.team.models.Team;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;


//Spring Annotation
@RestController
@RequestMapping("/team")
public class teamController {
    private List<Team> teams=new ArrayList<Team>();

    @GetMapping("/list")
    public List<Team> getList(){
        return teams;
    }

    @PostMapping("/add")
    public void addTeam(@RequestBody Team team){
        teams.add(team);
    }

    //JSON
    //{"name": "Jason",
    // "memberCount": 6}

}   